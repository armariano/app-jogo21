package br.com.itau;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Historico {
    public ArrayList<Integer> listaSoma = new ArrayList<Integer>();

    public void incluirSomaHistorico(int soma){
        listaSoma.add(soma);
    }

    public void visualizarHistorico(){
        Collections.sort(listaSoma);

        System.out.println("Top histórico: ");
        for (Integer item : listaSoma) {
            System.out.println(item);
        }
    }

}
