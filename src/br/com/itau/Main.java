package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int continuar = 1;
        Scanner scanner = new Scanner(System.in);
        Baralho baralho = new Baralho();
        Jogador jogador = new Jogador();
        Historico historico = new Historico();

        baralho.montarBaralho();

        System.out.println("------------------------ JOGO 21 ------------------------");

        do {
            System.out.println("1 - Pegar carta | 2 - Parar jogo | 3 - Histórico | 4 - Sair");
            int opcao = scanner.nextInt();

            if(opcao == 1){

                Carta carta = baralho.pegarCarta();
                int soma = jogador.somar(carta);
                System.out.println("Pontos: " + soma);

                jogador.validaSoma(soma);

            }else if(opcao == 2){
                historico.incluirSomaHistorico(jogador.getValorSoma());
                jogador.zerarValor();
                System.out.println("Jogo finalizado e pontuação armazenada em histórico.");
            }else if(opcao == 3){
                historico.visualizarHistorico();
            }else{
                System.out.println("JOGO ENCERRADO.");
                continuar = 4;
            }

        }while(continuar < 4);
    }
}
