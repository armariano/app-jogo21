package br.com.itau;

public class Jogador {
    public int valorSoma;
    public int valorEntrada;

    public Jogador() {
    }

    public int getValorSoma() {
        return valorSoma;
    }

    public void setValorSoma(int valorSoma) {
        this.valorSoma = valorSoma;
    }

    public int getValorEntrada() {
        return valorEntrada;
    }

    public void setValorEntrada(int valorEntrada) {
        this.valorEntrada = valorEntrada;
    }

    public int somar(Carta carta){

        if(carta.getValor() >= 10){
            valorEntrada = 10;
        }else{
            valorEntrada = carta.valor;
        }

        valorSoma += valorEntrada;

        return valorSoma;
    }

    public void validaSoma(int valorSoma){
        if(valorSoma > 21){
            System.out.println("Você perdeu!");
            zerarValor();
        }else{
            System.out.println("Parabéns");
        }
    }

    public void zerarValor(){
        setValorEntrada(0);
        setValorSoma(0);
    }
}
