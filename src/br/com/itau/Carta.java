package br.com.itau;

public class Carta {
    public static final String paus = "PAUS";


    protected int valor;
    protected int naipe;

    public Carta(int valor, int naipe) {
        this.valor = valor;
        this.naipe = naipe;
    }

    public Carta() {}

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getNaipe() {
        return naipe;
    }

    public void setNaipe(int naipe) {
        this.naipe = naipe;
    }

    public void identificarCarta(Carta carta){
        switch (carta.valor){
            case 11 :
                System.out.print("DAMAS de ");
                break;
            case 12 :
                System.out.print("VALETE de ");
                break;
            case 13 :
                System.out.print("REIS de ");
                break;
            case 1 :
                System.out.print("AIS de ");
                break;
            default :
                System.out.print(carta.valor + " de ");
        }

        switch (carta.naipe){
            case 1 :
                System.out.print(Naipe.PAUS);
                break;
            case 2 :
                System.out.println(Naipe.OUROS);
                break;
            case 3 :
                System.out.println(Naipe.ESPADAS);
                break;
            case 4 :
                System.out.println(Naipe.COPAS);
                break;
            default :
                System.out.println("");
        }
        System.out.println("");
    }
}
