package br.com.itau;

import java.util.ArrayList;
import java.util.Random;

public class Baralho {
    public ArrayList<Carta> baralhoMontado = new ArrayList<Carta>();

    public Baralho() {}

    public void montarBaralho(){

        for (int v = 1; v < 14; v++){
            Carta carta = new Carta();
            carta.valor = v;
            for (int n = 1; n < 5; n ++){
                carta.naipe = n;
                baralhoMontado.add(new Carta(v, n));
            }
        }
    }

    public Carta pegarCarta(){
        Carta carta = new Carta();
        Random random = new Random();

        int indice = random.nextInt(baralhoMontado.size()-1);

        carta = baralhoMontado.get(indice);

        carta.identificarCarta(carta);

        baralhoMontado.remove(indice);

        return carta;
    }
}
